package com.citi.training.employee.menu;


public class WrongTypeEnteredException extends Exception {

    public WrongTypeEnteredException(String message) {
        super(message);
    }
}
