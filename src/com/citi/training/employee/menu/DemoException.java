package com.citi.training.employee.menu;

public class DemoException extends RuntimeException {

    public DemoException(String message) {
        super(message);
    }
}
